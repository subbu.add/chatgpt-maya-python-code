import json
import pymel.core as pm

def export_skin_weights(mesh_list, file_path):
    data = {}
    for mesh in mesh_list:
        skin_cluster = pm.PyNode(mesh).getShape().listHistory(type='skinCluster')[0]
        influences = [inf.name() for inf in skin_cluster.influenceObjects()]
        weights = skin_cluster.getWeights(mesh)
        data[mesh.name()] ={}
        for i, weight_list in enumerate(weights):
            data[mesh.name()][i] = dict(zip(influences, weight_list))
    with open(file_path, 'w') as f:
        json.dump(data, f, indent=2)

# Create a list of mesh objects
mesh_list = nselected()

# Call the export function
export_skin_weights(mesh_list, "D:/My_Scripts/$_as_HyperSkin/$$-HyperSkin-RnD/skin_weights.json")
