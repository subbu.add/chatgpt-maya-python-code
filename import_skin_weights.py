import json
import pymel.core as pm

def import_skin_weights(json_file_path):
    # Load the JSON file
    json_file = open(json_file_path, 'r')
    json_data = json.load(json_file)
    
    # Iterate over each mesh in the JSON data
    for mesh_name, mesh_data in json_data.items():
        # Select the mesh in Maya
        pm.select(mesh_name)
        
        # Check if the selected mesh has a skin cluster
        skin_cluster = pm.listHistory(mesh_name, type='skinCluster')
        if not skin_cluster:
            print('Mesh "{}" does not have a skin cluster attached.'.format(mesh_name))
            continue
        
        # Iterate over each vertex in the mesh
        for vertex_num, vertex_weights in mesh_data.iteritems():
            # Set the skin weights for the vertex
            weightsList =[(jnt, value) for jnt, value in vertex_weights.items()]
            pm.skinPercent(skin_cluster[0], '{0}.vtx[{1}]'.format(mesh_name, vertex_num), normalize=True, transformValue=weightsList)

import_skin_weights(json_file_path="D:/My_Scripts/$_as_HyperSkin/$$-HyperSkin-RnD/skin_weights.json")
